module.exports = {
  parser: '@typescript-eslint/parser', // Specifies the ESLint parser
  parserOptions: {
    ecmaVersion: 2020, // Allows for the parsing of modern ECMAScript features
    sourceType: 'module', // Allows for the use of imports
    ecmaFeatures: {
      jsx: true // Allows for the parsing of JSX
    }
  },
  settings: {
    react: {
      version: 'detect' // Tells eslint-plugin-react to automatically detect the version of React to use
    }
  },
  extends: [
    'plugin:react/recommended', // Uses the recommended rules from @eslint-plugin-react
    'plugin:@typescript-eslint/recommended', // Uses the recommended rules from the @typescript-eslint/eslint-plugin
    'prettier/@typescript-eslint' // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
  ],
  rules: {
    'arrow-parens': [2, 'always'],
    'block-spacing': ['error', 'always'],
    'comma-dangle': ['error', 'never'],
    'dot-location': ['error', 'property'],
    'key-spacing': 2,
    'keyword-spacing': 2,
    'lines-between-class-members': ['error', 'always'],
    'no-alert': 2,
    'no-console': 2,
    'no-const-assign': 2,
    'no-debugger': 2,
    'no-dupe-args': 2,
    'no-dupe-keys': 2,
    'no-empty-pattern': 2,
    'no-eq-null': 2,
    'no-irregular-whitespace': 2,
    'no-mixed-operators': 2,
    'no-trailing-spaces': 2,
    'no-unreachable': 2,
    'no-var': 2,
    'padded-blocks': ['error', 'never'],
    'padding-line-between-statements': 2,
    'prefer-const': 2,
    'prefer-object-spread': 2,
    'sort-imports': [
      2,
      {
        allowSeparatedGroups: true,
        ignoreCase: true
      }
    ],
    eqeqeq: ['error', 'always'],
    quotes: ['error', 'single'],
    semi: ['error', 'never'],

    // react
    'react/prefer-es6-class': 2,
    'react/react-in-jsx-scope': 'off',
    'react/self-closing-comp': [
      'error',
      {
        component: true,
        html: true
      }
    ],
    'react/sort-comp': [
      2,
      {
        order: ['static-methods', 'lifecycle', '/^on.+$/', 'everything-else', 'render']
      }
    ],
    'react/state-in-constructor': [2, 'always'],
    'react/void-dom-elements-no-children': 2,

    // JSX
    'react/jsx-child-element-spacing': 2,
    'react/jsx-closing-bracket-location': 2,
    'react/jsx-curly-brace-presence': [2, { props: 'never', children: 'never' }],
    'react/jsx-curly-newline': [2, { multiline: 'forbid', singleline: 'forbid' }],
    'react/jsx-first-prop-new-line': [2, 'multiline'],
    'react/jsx-newline': 2,
    'react/jsx-no-bind': 0,
    'react/jsx-no-undef': 2,
    'react/jsx-pascal-case': 2,
    'react/jsx-props-no-multi-spaces': 2,
    'react/jsx-sort-default-props': 2,
    'react/jsx-sort-props': [
      2,
      {
        callbacksLast: true,
        shorthandFirst: false,
        shorthandLast: false,
        ignoreCase: true
      }
    ],
    'react/jsx-uses-react': 2,
    'react/jsx-wrap-multilines': 2,

    // typescript
    '@typescript-eslint/ban-ts-comment': 0,
    '@typescript-eslint/no-empty-interface': 0,
    '@typescript-eslint/no-explicit-any': 0,
    '@typescript-eslint/no-unused-vars': 2
  }
}
