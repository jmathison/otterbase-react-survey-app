import * as yup from 'yup'
import { AppBar, Grid, Link, Paper, TextField, Toolbar } from '@material-ui/core'
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles'
import { Formik, FormikProps, FormikValues } from 'formik'
import Button from '@material-ui/core/Button'
import React from 'react'
import Step from '@material-ui/core/Step'
import StepLabel from '@material-ui/core/StepLabel'
import Stepper from '@material-ui/core/Stepper'
import Typography from '@material-ui/core/Typography'

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    appBar: {
      position: 'relative'
    },
    layout: {
      width: 'auto',
      marginLeft: theme.spacing(2),
      marginRight: theme.spacing(2),
      // eslint-disable-next-line no-mixed-operators
      [theme.breakpoints.up(600 + theme.spacing(2) * 2)]: {
        width: 600,
        marginLeft: 'auto',
        marginRight: 'auto'
      }
    },
    paper: {
      marginTop: theme.spacing(3),
      marginBottom: theme.spacing(3),
      padding: theme.spacing(2),
      // eslint-disable-next-line no-mixed-operators
      [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
        marginTop: theme.spacing(6),
        marginBottom: theme.spacing(6),
        padding: theme.spacing(3)
      }
    },
    stepper: {
      padding: theme.spacing(2, 0)
    },
    button: {
      marginRight: theme.spacing(1)
    },
    buttons: {
      display: 'flex',
      justifyContent: 'flex-end',
      marginTop: theme.spacing(3)
    },
    instructions: {
      marginTop: theme.spacing(1),
      marginBottom: theme.spacing(1)
    }
  })
)

function getSteps() {
  return ['Personal', 'Professional', 'Contact']
}

function getStepContent(step: number, formik: FormikProps<any>) {
  switch (step) {
    case 0:
      return StepOne(formik)
    case 1:
      return StepTwo(formik)
    case 2:
      return StepThree(formik)
    default:
      return <Typography>Unknown step</Typography>
  }
}

function StepOne({ values, handleChange, handleBlur, touched, errors }: FormikProps<any>) {
  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <TextField
          autoFocus
          error={touched.firstName && Boolean(errors.firstName)}
          fullWidth
          id="firstName"
          label="First name"
          name="firstName"
          required
          value={values.firstName}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </Grid>

      <Grid item xs={12}>
        <TextField
          error={touched.lastName && Boolean(errors.lastName)}
          fullWidth
          id="lastName"
          label="Last name"
          name="lastName"
          required
          value={values.lastName}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </Grid>

      <Grid item xs={12}>
        <TextField
          error={touched.email && Boolean(errors.email)}
          fullWidth
          id="email"
          label="Email"
          name="email"
          required
          type="email"
          value={values.email}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </Grid>
    </Grid>
  )
}

function StepTwo({ values, handleChange, handleBlur, touched, errors }: FormikProps<any>) {
  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <TextField
          error={touched.yearsOfExperience && Boolean(errors.yearsOfExperience)}
          fullWidth
          id="yearsOfExperience"
          label="Years of experience"
          name="yearsOfExperience"
          required
          type="number"
          value={values.yearsOfExperience}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </Grid>

      <Grid item xs={12}>
        <TextField
          error={touched.hourlyRateExpectation && Boolean(errors.hourlyRateExpectation)}
          fullWidth
          id="hourlyRateExpectation"
          label="Hourly rate expectation"
          name="hourlyRateExpectation"
          required
          type="number"
          value={values.hourlyRateExpectation}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </Grid>

      <Grid item xs={12}>
        <TextField
          error={touched.aboutYou && Boolean(errors.aboutYou)}
          fullWidth
          id="aboutYou"
          label="About you"
          multiline
          name="aboutYou"
          required
          rows={4}
          value={values.aboutYou}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </Grid>
    </Grid>
  )
}

function StepThree({ values, handleChange, handleBlur, touched, errors }: FormikProps<any>) {
  return (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <TextField
          error={touched.linkedin && Boolean(errors.linkedin)}
          fullWidth
          id="linkedin"
          label="LinkedIn"
          name="linkedin"
          value={values.linkedin}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </Grid>

      <Grid item xs={12}>
        <TextField
          error={touched.github && Boolean(errors.github)}
          fullWidth
          id="github"
          label="GitHub"
          name="github"
          value={values.github}
          onBlur={handleBlur}
          onChange={handleChange}
        />
      </Grid>
    </Grid>
  )
}

function Copyright() {
  return (
    <Typography align="center" color="textSecondary" variant="body2">
      {'Copyright © '}

      <Link color="inherit" href="https://google.com/">
        React survey test
      </Link>{' '}

      {new Date().getFullYear()}.
    </Typography>
  )
}

export default function HorizontalLinearStepper(): JSX.Element {
  const classes = useStyles()
  const [activeStep, setActiveStep] = React.useState(0)
  const steps = getSteps()

  const handleNext = ({ values, validateForm }: FormikProps<any>) => {
    setTimeout(() => {
      setActiveStep((prevActiveStep) => prevActiveStep + 1)

      const input = window.document.querySelector('input')
      input?.focus()

      validateForm(values)
    }, 250)
  }

  const handleBack = ({ values, validateForm }: FormikProps<any>) => {
    setTimeout(() => {
      setActiveStep((prevActiveStep) => prevActiveStep - 1)

      const input = window.document.querySelector('input')
      input?.focus()

      validateForm(values)
    }, 250)
  }

  const handleReset = () => {
    setActiveStep(0)
  }

  const handleSubmit = (data: FormikValues) => {
    // eslint-disable-next-line no-console
    console.log(data)
  }

  const getValidationSchema = () => {
    switch (activeStep) {
      case 0:
        return yup.object().shape({
          firstName: yup.string().max(50).required(),
          lastName: yup.string().max(50).required(),
          email: yup.string().email().required()
        })
      case 1:
        return yup.object().shape({
          yearsOfExperience: yup.number().min(0).required(),
          hourlyRateExpectation: yup.number().min(0).required(),
          aboutYou: yup.string().max(500).required()
        })
      case 2:
        return yup.object().shape({
          linkedin: yup.string().max(100),
          github: yup.string().max(100)
        })
      default:
        return undefined
    }
  }

  return (
    <>
      <AppBar className={classes.appBar} color="default" position="absolute">
        <Toolbar>
          <Typography color="inherit" noWrap variant="h6">
            React survey test
          </Typography>
        </Toolbar>
      </AppBar>

      <main className={classes.layout}>
        <Paper className={classes.paper}>
          <Typography align="center" component="h1" variant="h4">
            Please complete
          </Typography>

          <Stepper activeStep={activeStep} className={classes.stepper}>
            {steps.map((label) => {
              const stepProps: { completed?: boolean } = {}

              return (
                <Step key={label} {...stepProps}>
                  <StepLabel>{label}</StepLabel>
                </Step>
              )
            })}
          </Stepper>

          {activeStep === steps.length ? (
            <div>
              <Typography className={classes.instructions}>All steps completed - you&apos;re finished</Typography>

              <Button className={classes.button} onClick={handleReset}>
                Reset
              </Button>
            </div>
          ) : (
            <div>
              <Formik
                initialValues={{
                  firstName: '',
                  lastName: '',
                  email: '',
                  yearsOfExperience: '',
                  hourlyRateExpectation: '',
                  aboutYou: '',
                  linkedin: '',
                  github: ''
                }}
                validateOnMount
                validationSchema={getValidationSchema()}
                onSubmit={handleSubmit}
              >
                {(formik) => (
                  <form autoComplete="off" noValidate onSubmit={formik.handleSubmit}>
                    {getStepContent(activeStep, formik)}

                    <div className={classes.buttons}>
                      <Button className={classes.button} disabled={activeStep === 0} onClick={() => handleBack(formik)}>
                        Back
                      </Button>

                      <Button
                        className={classes.button}
                        color="primary"
                        disabled={!formik.isValid}
                        type={activeStep === steps.length - 1 ? 'submit' : 'button'}
                        variant="contained"
                        onClick={() => handleNext(formik)}
                      >
                        {activeStep === steps.length - 1 ? 'Submit' : 'Next'}
                      </Button>
                    </div>
                  </form>
                )}
              </Formik>
            </div>
          )}
        </Paper>

        <Copyright />
      </main>
    </>
  )
}
